/*
    THE END ♪♫ - Thanks for playing!
*/
import { saveScore } from './components/scores/scores';
import { getScore } from './components/scores/scores';

function stop() {
    if (sessionStorage.getItem('stateGame')) {
        saveScore();
        sessionStorage.removeItem('stateGame');
    }
    getScore();
}
stop();