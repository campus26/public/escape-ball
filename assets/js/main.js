/* 
    CALLS AND RUN THE GAME
*/


/*
    Adds player name into the game
*/

document.querySelector("#name").innerText = sessionStorage.getItem("name");

/*
    Local storage Init


import { saveScores } from './components/scores/scores';
if (sessionStorage.getItem('time');)
saveScores();*/

/* 
    Game engine and render Init
*/

import { engine, render } from './components/gameEngine';
import { increaseTime } from './components/timer/timer';


Engine.run(engine);
Render.run(render);

/* 
    Levels engine Init
*/

import { levelsEngine } from './components/levels/levelsEngine';

/*
    Game Init
*/

function start() {
    levelsEngine();
    setInterval(increaseTime, 10);
}
start(); // Let's play!