/*
    CREATES THE ENGINE AND THE RENDER
*/

export let engine = Engine.create();

export let render = Render.create({
    element: document.body,
    engine: engine,
    options: {
        width: 1920,
        height: 1080,
        pixelRatio: 1,
        background: 'black',
        wireframeBackground: '#222',
        hasBounds: false,
        enabled: true,
        wireframes: false,
        showAngleIndicator: true,
        showCollisions: true,
        showVelocity: true
    }
});

window.onresize = () => {
    render.options.width = window.innerWidth;
    render.options.height = window.innerHeight;
    document.querySelector("canvas").width = window.innerWidth;
    document.querySelector("canvas").height = window.innerHeight;
}