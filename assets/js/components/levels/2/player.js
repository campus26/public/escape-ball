/* 
    CREATES THE PLAYER AND THE WAY TO CONTROL IT (THEREFORE THE CAMERA MOVEMENTS)
*/
import { engine } from '../../gameEngine';
import { keyboard } from '../../inputs/inputsManager';

export let playerX = 0; // export to use it for the camera's starting point
export let playerY = 400; // export to use it for the camera's starting point
let playerRadius = 40;
let angularVelocityToSet = 0;
let xVelocityToSet = 0;
let jumpVelocity = 0;

/*
    Creates the player
*/

let sprite = [];
for (let i = 7; i >= 0; i--) {
    sprite[i] = new Image();
    sprite[i].src = 'assets/sprites/ball-' + i + '.png';
};

export let player = Bodies.circle(playerX, playerY, playerRadius, {  //x,y,taille
    id: "player",
    friction: 0.5,
    render: { // options
        sprite: {
            texture: 'assets/sprites/ball-0.png',
            xScale: playerRadius / (149 / 2),
            yScale: playerRadius / (149 / 2)
        }
    }
});

/*
Controls the player and the camera (as it follows the player)
*/

let xTrigo = Math.cos(player.angle);
let yTrigo = Math.sin(player.angle);
let spriteNumber = 0;

window.addEventListener("keydown", (keyPressed) => {
    console.log(keyPressed.key);
});

// detecting if the player is in air
let collisionAngles = [];
let onGround = true;
Events.on(engine, 'collisionStart', (collisionTriggered) => {
    
    collisionTriggered.pairs.forEach((pair) => {
        
        if (pair.bodyB.id == "player") {
            
            [pair.bodyA, pair.bodyB] = [pair.bodyB, pair.bodyA];
        }
        if (pair.bodyA.id == "player") {

            let actualTangent = pair.collision.tangent;
            collisionAngles[pair.id] = actualTangent;
            collisionAngles.push(actualTangent);
        }
    });
});
Events.on(engine, 'collisionEnd', (collisionTriggered) => {
    
    collisionTriggered.pairs.forEach((pair) => {

        // Making sure our bodyA is player, if not, swap between them, because we want the player to be the bodyA
        if (pair.bodyB.id == "player") {

            [pair.bodyA, pair.bodyB] = [pair.bodyB, pair.bodyA];
        }
        if (pair.bodyA.id == "player") {

            collisionAngles.splice(collisionAngles.indexOf(collisionAngles[pair.id]), 1);
            delete collisionAngles[pair.id];
        }
    });
});

let jumpDelay = 0;
// add our code to the event loop
Events.on(engine, 'beforeUpdate', () => {

    // Player movements
    collisionAngles.forEach((angle) => {

        if (angle.x < -0.1) {
            
            onGround = true;
            jumpVelocity = 0;
            xVelocityToSet = player.velocity.x;
        }
    });
    if (collisionAngles.length == 0) {
        onGround = false;
    }
    if (!onGround) {
        angularVelocityToSet *= 0.95;
    }

    jumpDelay = onGround ? 0 : jumpDelay + 1;
    if (keyboard.up.pressed) {
        if (onGround || jumpDelay < 9) {
            jumpVelocity -= 1;
            Body.setVelocity(player, { x: player.velocity.x, y: jumpVelocity });
        }
    } else {
        jumpVelocity = player.velocity.y;
    }
    // console.log(jumpDelay);

    if (keyboard.left.pressed) {
        if (onGround) {
            
            if (!keyboard.right.pressed && angularVelocityToSet > 0) {
                angularVelocityToSet = 0;
            }
            angularVelocityToSet -= 0.01;
        } else {

            if (!keyboard.right.pressed && xVelocityToSet > 0) {
                xVelocityToSet *= 0.8;
            }
            xVelocityToSet -= 0.5;
        }
    }
    
    if (keyboard.right.pressed) {

        if (onGround) {

        if (!keyboard.left.pressed && angularVelocityToSet < 0) {
            angularVelocityToSet = 0;
        }
        angularVelocityToSet += 0.01;
        } else {
            if (!keyboard.left.pressed && xVelocityToSet < 0) {
                xVelocityToSet *= 0.8;
            }
            xVelocityToSet += 0.5;
        }
    }

    if (!keyboard.left.pressed && !keyboard.right.pressed) {
        angularVelocityToSet = player.angularVelocity;
        xVelocityToSet = player.velocity.x;
    }

    // console.log(angularVelocityToSet);

    if (keyboard.down.pressed) {
        angularVelocityToSet *= 0.8;
        xVelocityToSet *= 0.8;
    };

    if (keyboard.left.pressed || keyboard.right.pressed || keyboard.down.pressed || keyboard.up.pressed) {

        if (angularVelocityToSet >= 0.4) {
            angularVelocityToSet = 0.4;
            // console.log("CAPPED +");
        }
        if (angularVelocityToSet <= -0.4) {
            angularVelocityToSet = -0.4;
            // console.log("CAPPED -");
        }

        if (xVelocityToSet >= 10) {
            xVelocityToSet = 10;
            // console.log("CAPPED +");
        }
        if (xVelocityToSet <= -10) {
            xVelocityToSet = -10;
            // console.log("CAPPED -");
        }
        // console.log("xVelocityToSet = " + xVelocityToSet);
        console.log("player.velocity.x = " + player.velocity.x);

        // console.log(angularVelocityToSet);
        Body.setAngularVelocity(player, angularVelocityToSet);
        Body.setVelocity(player, { x: xVelocityToSet, y: player.velocity.y })
    }

    //* Player sprites

    xTrigo = Math.cos(player.angle);
    yTrigo = Math.sin(player.angle);
    // console.log(spriteNumber, player.angle.toFixed(2), xTrigo.toFixed(2), yTrigo.toFixed(2));
    
    spriteNumber = 0;
    if (yTrigo > 0) { // la balle a fait -0.5 tour
        if (xTrigo < 0) { // la balle a fait +0.25 tour
            spriteNumber += 2;
        }
    } else { // la balle a fait +0.5 tour
        if (xTrigo > 0) { // la balle a fait +0.75 tour
            spriteNumber += 2;
        }
        spriteNumber += 4;
    }


    // angle 1
    if (xTrigo < -0.75 && yTrigo > 0) {
        spriteNumber = "4";
        // player.render.sprite.texture = './img/ball-4.PNG';
        // console.log(player.render.sprite.texture, "3");
    }    
    // angle 2
    if (xTrigo > -0.75 && xTrigo < 0 && yTrigo > 0) {
        spriteNumber = "3";
        // player.render.sprite.texture = './img/ball-3.PNG';
        // console.log(player.render.sprite.texture, "2");
    }
    // angle 3
    if (xTrigo > 0 && xTrigo < 0.75 && yTrigo > 0) {
        spriteNumber = "2";
        // player.render.sprite.texture = './img/ball-2.PNG';
        // console.log(player.render.sprite.texture, "1");
    }

    // angle 4 (départ x=1)
    if (xTrigo > 0.75 && yTrigo > 0) {
        spriteNumber = "1";
        // player.render.sprite.texture = './img/ball-1.PNG';
        // console.log(player.render.sprite.texture, "0");
    }
    
    // angle 5
    if (xTrigo > 0.75 && yTrigo < 0) {
        spriteNumber = "0";
        // player.render.sprite.texture = './img/ball-0.PNG';
        // console.log(player.render.sprite.texture, "7");
    }

    // angle 6
    if (xTrigo > 0 && xTrigo < 0.75 && yTrigo < 0) {
        spriteNumber = "7";
        // player.render.sprite.texture = './img/ball-7.PNG';
        // console.log(player.render.sprite.texture, "6");
    }

    // angle 7
    if (xTrigo > -0.75 && xTrigo < 0 && yTrigo < 0) {
        spriteNumber = "6";
        // player.render.sprite.texture = './img/ball-6.PNG';
        // console.log(player.render.sprite.texture, "5");
    }

    // angle 8
    if (xTrigo < -0.75 && yTrigo < 0) {
        spriteNumber = "5";
        // player.render.sprite.texture = './img/ball-5.PNG';
        // console.log(player.render.sprite.texture, "4");
    }
    
    // console.log(spriteNumber, player.angle.toFixed(2), xTrigo.toFixed(2))
    player.render.sprite.texture = 'assets/sprites/ball-' + spriteNumber + '.png';
});
