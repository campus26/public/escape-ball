let group = Body.nextGroup(true);

let ropeC = Composites.stack(600, 0, 1, 14, 0, -15, function(x, y) {
    console.log(group);
    return Bodies.rectangle(x, y, 20, 30, {
        collisionFilter: { group: group },
        chamfer: 5,
        render: {fillStyle: "yellow"},
        force: {x:-0.0025,y:0}
    });
});


let plug = Bodies.rectangle(
    ropeC.bodies[ropeC.bodies.length - 1].position.x,
    ropeC.bodies[ropeC.bodies.length - 1].position.y,
    30,
    30,
    { collisionFilter: { group: group } }
);

Composite.add(ropeC, plug);
    
Composites.chain(
    ropeC,
    0,
    0.2,
    0,
    -0.2,
    { stiffness: 1, length: 0, render: { visible: false } }
);

export let electricalPlug = Composite.add(ropeC, Constraint.create({ 
    bodyB: ropeC.bodies[0],
    pointB: { x: 0, y: -20 },
    pointA: { x: ropeC.bodies[0].position.x, y: ropeC.bodies[0].position.y - 20},
    stiffness: 1,
    damping: 1,
}));
