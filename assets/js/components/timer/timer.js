let startTime = (new Date()).getTime();
const timeElement = document.querySelector("#time");

export const increaseTime = () => {
    let t = (new Date()).getTime() - startTime;

    let totalSecond = Math.floor(t / 1000);

    let m = Math.floor(totalSecond / 60);
    let s = totalSecond % 60;
    let ms = t % 1000; 
    m = m < 10 ? `0${m}` : m;
    s = s < 10 ? `0${s}` : s;
    ms = ms < 10 ? `00${ms}` : ms;
    ms = ms < 100 ? `0${ms}` : ms;
    timeElement.innerText = `${m}:${s}:${ms}`;
  };