/*
    DEFINES THE KEYBOARD PLAYER CONTROL
*/

/*
Look at the comments below to add your own object to control
*/

// This is the Input class that manages the events listener and callbacks. DO NOT REMOVE.
import { Input } from './inputsEngine';

export let keyboard = {
    up: new Input(['z', 'Z', 'w', 'W', 'ArrowUp', ' ']),
    down: new Input(['s', 'S', 'ArrowDown', 'Control']),
    left: new Input(['q', 'Q', 'a', 'A', 'ArrowLeft']),
    right: new Input(['d', 'D', 'ArrowRight']),
}
