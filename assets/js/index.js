/*
    JS just to get the player's name from index.html, send it into a session and then start the game
 */

document.querySelector("form").addEventListener("submit", function (e) {
    // don't send the form please!
    e.preventDefault();

    // instead, send name to session storage
    let playerName = document.querySelector("#name").value;
    sessionStorage.setItem('name', playerName);
    sessionStorage.setItem('stateGame', 1);

    
    // good boy! Now you can start the game!
    document.location.href="game.html";
});

